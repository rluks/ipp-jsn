#!/usr/bin/env perl

#JSN:xluksr00

##########################
# Projekt do IPP (perl)
# Zadani 3: JSN
# Jmeno: Roman Luks
# Login: xluksr00
# 2011/2012 FIT VUTBR
##########################

use strict;
use warnings;
use locale;

my $ladeni = 1; #prepinace
my $ladeni_s = 1; #soubory
if($ladeni){print "################### BEGIN skript ###################\n";}

# inicializace prepinacu prikazove radky
my @argumenty = @ARGV;
my $help = '';
my $input = '';
my $output = '';
my $prepinac_n = '';
my $prepinac_r = '';
my $array_name = '';
my $item_name = '';
my $prepinac_s = '';
my $prepinac_i = '';
my $prepinac_c = '';
my $array_size = '';
my $index_items = '';
my $start = '';

# zpracovani prepinacu prikazove radky
use Getopt::Long;
Getopt::Long::Configure ("bundling");
my $result = GetOptions(
"help+" => \$help, 
"input=s" => \$input, 
"output=s" => \$output, 
"n+" => \$prepinac_n,
"r=s" => \$prepinac_r,
"array-name=s" => \$array_name,
"item-name=s" => \$item_name,
"s+" => \$prepinac_s,
"i+" => \$prepinac_i,
"c+" => \$prepinac_c,
"a|array-size+" => \$array_size,
"t|index-items+" => \$index_items,
"start=i" => \$start);

if($ladeni){print "Hodnota GetOptions byla: ", $result, "\n";}
#kontrola na neznamy parametr
if($result eq ''){Errors::printBadArgs();}

# TODO rir(remove in release)
if($ladeni){
print "### BEGIN Prepinace: ###\n";
print "help: $help\n";
print "input: $input\n";
print "output: $output\n";
print "n: $prepinac_n\n";
print "r: $prepinac_r\n";
print "array_name: $array_name\n";
print "item_name: $item_name\n";
print "s: $prepinac_s\n";
print "i: $prepinac_i\n";
print "c: $prepinac_c\n";
print "array_size: $array_size\n";
print "index_items: $index_items\n";
print "start: $start\n";
}

#kontrola vicenasobneho pouziti kratkych prepinacu
  if($ladeni){print "#kontrola vicenasobneho pouziti kratkych prepinacu\n";}
  my @kratke_prepinace = ($help, $prepinac_n, $prepinac_s, $prepinac_i,
                   $prepinac_c, $array_size, $index_items);
  
  #TODO rir
  if($ladeni){
    my $length = (scalar @kratke_prepinace) - 1;
    #my $size = $#prepinace + 1;
    #print $size, "\n";
    print "delka pole prepinacu (help, n, s, i, c, array-size, index-item) je: ", $length+1, "\n";
    for my $i (0 .. $length){print $i, ". prvek pole:", $kratke_prepinace[$i], "\n";}
    #my $i = 0;
    #for my $kontrola (@prepinace){print $i, ". prvek pole:", $kontrola, "\n";$i = $i + 1;}
  }
  
  for my $kontrola(@kratke_prepinace){
    if($kontrola gt 1){Errors::printBadArgs();}
  }
  #TODO rir
  if($ladeni){print "Kontrola vicenasobneho pouziti kratkych prepinacu probehla v poradku\n";}
#OK-kratke prepinace

#kontrola vicenasobneho pouziti dlouhych prepinacu/s hodnotou
  if($ladeni){print "#kontrola vicenasobneho pouziti dlouhych prepinacu/s hodnotou\n";}
  my $length_a = (scalar @argumenty) - 1;
  if($ladeni){print "### rozparsovane prepinace: ####\n";for my $i(0 .. $length_a){print "argument c. ", $i, ":", $argumenty[$i], "\n";}}

  #Do pole pro kontrolu dlouhych prepinacu vlozime jen pouzivane prepinace
  my @dlouhe_prepinace;
  if($input){push @dlouhe_prepinace, "--input=";}
  if($output){push @dlouhe_prepinace, "--output=";}
  if($prepinac_r){push @dlouhe_prepinace, "-r=";}
  if($array_name){push @dlouhe_prepinace, "--array-name=";}
  if($start){push @dlouhe_prepinace, "--start=";}
  #my @dlouhe_prepinace = qw(--input= --output= -r= --array-name= --item-name= --start=);
  my $length_dp = (scalar @dlouhe_prepinace) - 1;
  if($ladeni){print "### pole dlouhych prepinacu/s hodnotou: ####\n";for my $i(0 .. $length_dp){print "parametr c. ", $i, ":", $dlouhe_prepinace[$i], "\n";}}
    
  for my $i(0 .. $length_dp){
    my $pocitadlo_shod = 0;#inicializace pocitadla pro kazdy prepinac
    for my $j(0 .. $length_a){
      if($ladeni){print "Porovnavam ", $i, ". prepinac:", $dlouhe_prepinace[$i]," s aktualnim ", $j, ". argumentem skriptu:", $argumenty[$j], " :";}
      if($argumenty[$j] =~ m/($dlouhe_prepinace[$i])/){
        if($ladeni){print "SHODA \n";}
        $pocitadlo_shod++;
        if($pocitadlo_shod > 1){Errors::printBadArgs();}
      }else
      {if($ladeni){print ".......................................neshoduji se \n";}}
    }
  }
  
  #byl nastaven prepinac input
  if($input){
    if($ladeni){print "#byl nastaven prepinac input\n";print "input je:", $input, "\n";}#TODO rir
    my $pocitadlo_shod = 0;
    for my $i(0 .. $length_a){
      if($ladeni){print "argument c. ", $i, ":", $argumenty[$i], "\n";}#TODO rir
      #Parametr obsahuje podretezec "--input="
      if($argumenty[$i] =~ m/--input=/){
        $pocitadlo_shod++;
        if($ladeni){print "Pocitadlo shod:", $pocitadlo_shod, "\n";}#TODO rir
        if($pocitadlo_shod > 1){Errors::printBadArgs();}
      }
    }
  }
#OK-dlouhe prepinace

#byl nastaven prepinac help
if($help){
  if($ladeni){print "#byl nastaven prepinac help\n";}
  #help musi byt na prikazove radce samostatne - jinak exit 1
  my $length = scalar @argumenty;
  if($ladeni){print "Delka pole prepinacu je: ", $length, "\n";}
  if($length > 1){Errors::printBadArgs();}

  #OK => tisk help zpravy 
  print << 'KONEC';
  ##########################
  # Projekt do IPP (perl)
  # Zadani 3: JSN
  # Jmeno: Roman Luks
  # Login: xluksr00
  # 2011/2012 FIT VUTBR
  ##########################
  Prepinace:
  --help:           zobrazi tuto napovedu, jediny argument prikazove radky - jinak chyba (exit 1)
  --input=filename: vstupni soubor JSON v kodovani UTF-8, relativni nebo absolutni cesta 
                    (obsahuje-li mezery musi byt v uvozovkach)
                    chybi-li tento parametr, tak je vstupem STDIN
  --output=filename:vystupni soubor textovy XML, relativni nebo absolutni cesta 
                    (obsahuje-li mezery musi byt v uvozovkach)
                    chybi-li tento parametr, tak je vystupem STDOUT, existuje-li uz soubor s timto nazvem - je prepsan
  
  -n:               negenerovat XML hlavicku na vystup skriptu (vhodne napriklad v pripade kombinovani vice vysledku
                    hlavicka: <?xml version="1.0" encoding="UTF-8"?>
  
  -r=root-element:  jmeno paroveho korenoveho elementu obalujici vysledek, pokud nebude zadan, 
                    tak se vysledek neobaluje korenovym elementem
                    zadany root-element vedouci na nevalidni XML ukonci skript s chybou (exit 50)
  
  --array-name=array-element: prejmenovat element obalujici pole z implicitni hodnoty array na array-element, 
                              zadani elementu vedouci na nevalidni XML ukonci skript s chybou (exit 50)
  --item-name=item-element:   analogicky z implicitniho item na item-element, 
                              zadani elementu vedouci na nevalidni XML ukonci skript s chybou (exit 50)
  
  -s:               hodnoty dvojic typu string budou transformovany na textove elementy misto atributu
  -i:               hodnoty dvojic typu number budou transformovany na textove elementy misto atributu
  -l:               hodnoty literalu(true, false, null) budou transformovany
                    na elementy <true/>,<false/> a <null/> misto na atributy
  
  -c:               preklad problematickych znaku UTF-8 s kodem mensich jak 128
                    ve vstupnich retezcovych hodnotach na odpovidajici zapis v XML pomoci & (&amp; &lt; &gt; atd)
  
  -a, --array-size: u pole bude doplnen atribut size s uvedenim poctu prvku v tomto poli
  -t, --index-items:ke kazdemu prvku pole bude pridan atribut index
                    s urcenim indexu prvku v tomto poli (cislovani do 1 nebo parametrem --start)
  --start=n:        inicializace inkrementalniho citace pro indexaci prvku pole 
                    (nutne kombinovat s parametrem --index-items), 
                    na kladne cele cislo n (vc. nuly)
  JPD: --padding:   doplneni takoveho poctu nul zleva, aby vsechny cisla dane sekvence
                    mela stejny pocet cislic, ale minimalni mozny
  
  FLA: --flattening:neimplementovano
KONEC

}#if(help)

#implicitni hodnota n pro --start=n, n = 1
if(!$start){$start = 1;}
if($ladeni){print "Hodnota start je nastavena na: ", $start, "\n";}

if($ladeni){print "### END Prepinace ###\n";}
if($ladeni_s){print "### BEGIN Soubory ###\n";}

no strict;
use Fcntl; #kvuli MODE funkce sysopen() 

#pokud je definovan vstupni soubor, tak ho otevri - jinak cti STDIN
if($input){
  if(!sysopen(INFILE, $input, O_RDONLY)){
    if($ladeni_s){print "INFILE se nezdaril\n";}
    use Errors; 
    Errors::printBadFile();
  }
}
else{
  open(INFILE, "<-")
  or die "Error: otevreni STDIN se nezdarilo\n";#TODO
}

#pokud je definovan vystupni soubor, tak ho otevri - jinak pouzij STDOUT
if($output){
  if(!sysopen(OUTFILE, $output, O_WRONLY | O_CREAT | O_TRUNC)){
    if($ladeni_s){print "OUTFILE se nezdaril\n";}
    use Errors;
    Errors::printBadFile();
  }
}
else{
  open(OUTFILE, ">-")
  or die "Error: otevreni STDOUT se nezdarilo\n";#TODO
}

print "### Test zapisu do souboru ###\n";
print OUTFILE <INFILE>;


close(INFILE);
close(OUTFILE);
#$prumer = prumer(6, 9, 33);

#sub prumer {
    #my($a, $b, $c) = @_;
    #return ($a + $b + $c) / 3;
#} 

#no strict;

if($ladeni){print "################### END skript ###################";}
print "\n";
